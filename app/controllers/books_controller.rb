class BooksController < ApplicationController
  
  def list
    @books = Book.all
  end
  
  def show
    input = params['search']
    search = input.downcase.tr(" ", "_")
    apikey = "AIzaSyBQafM1TXCfHIVMdfB340ozGS7WFhaj5DA"
    base_url = "https://www.googleapis.com/books/v1/volumes?q=" + search +"+inauthor:keyes&key="+ apikey
    res = HTTParty.get(base_url)
    @result = JSON.parse(res.body)
    @searched = input
  end
  
  def delete
    
  end

  def new
    @book = Book.new
  end
  
  def create
    @book = Book.new(book_params)
    if @book.save
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def book_params
    params.require(:book).permit(:thumbnail, :title, :author)
  end
end
